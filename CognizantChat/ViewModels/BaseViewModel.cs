﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CognizantChat.Utils;
using Xamarin.Forms;

namespace CognizantChat.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public BaseViewModel()
        {
        }

        public virtual void OnAppearing()
        {

        }
    }
}