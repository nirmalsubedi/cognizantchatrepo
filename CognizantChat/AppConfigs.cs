﻿using System;
namespace CognizantChat
{
    public class AppConfigs
    {
        public bool EnableFake = true;
        public const string OfflineDbName = "CognizantChatDb.db3";

        private static readonly AppConfigs _instance = new AppConfigs();

        public static AppConfigs Instance
        {
            get
            {
                return _instance;
            }
        }

        private AppConfigs()
        {

        }
    }
}
