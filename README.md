

# Whats covered:
 -  Proper Xamarin Forms Structure and Convention
 -  Dependency Injection
 -  Fake Service Implementations 
 -  Full Support for Unit Tests
 -  Design time Live Xaml Preview
 -  Design time Bindings
 -  SQLite
 -  Custom Classes to support MVVM

# Whats not covered:
 -  Existing MVVM Libraries like FreshMvvm, MvvmCross or MvvmLight
 -  Error / Crash / Log Tracking using App Center
 -  etc


# Integrated the App Center Auto Build (CI/CD) 
 - https://appcenter.ms/users/NirmalSubedi/apps/ChatAppiOS/build/branches/master