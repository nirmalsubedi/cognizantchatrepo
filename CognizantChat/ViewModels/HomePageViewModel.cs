﻿using System;
using System.Collections.ObjectModel;
using CognizantChat.Models;
using CognizantChat.Services;
using Xamarin.Forms;

namespace CognizantChat.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        private readonly IChatService chatService;

        private ObservableCollection<ChatUser> chatUsers;
        public ObservableCollection<ChatUser> ChatUsers
        {
            get { return chatUsers; }
            set
            {
                chatUsers= value;
                OnPropertyChanged();
            }
        }

        public HomePageViewModel()
        {
            this.chatService = ServiceLocator.Instance.Resolve<IChatService>();

            if(DesignMode.IsDesignModeEnabled)
            {
                OnAppearing(); //load the data for design time.
            }
        }

        public override async void OnAppearing()
        {
            base.OnAppearing();

            //ToDo Handle other error conditions etc..

            await chatService.Init();

            var result = await chatService.GetChatUsers();
            ChatUsers = new ObservableCollection<ChatUser>(result);
        }
    }
}
