﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CognizantChat.Models;

namespace CognizantChat.Services
{
    public interface IChatService
    {
         Task<bool> Init();
         Task<List<ChatUser>> GetChatUsers(string query=null);
    }
}
