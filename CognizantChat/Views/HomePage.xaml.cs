﻿using System;
using System.Collections.Generic;
using CognizantChat.ViewModels;
using Xamarin.Forms;

namespace CognizantChat.Views
{
    public partial class HomePage : ContentPage
    {
        //ToDo We are not using any MVVM Framework like MvvmCross or FreshMvvm or anything.
        //We are managing our viewmodel initialization logic from the page here;

        HomePageViewModel vm;

        public HomePageViewModel ViewModel
        {
            get => vm; set
            {
                vm = value;
                BindingContext = vm;
            }
        }

        public HomePage()
        {
            InitializeComponent();
            if (vm == null)
                ViewModel = new HomePageViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vm?.OnAppearing();
        }
    }
}
