﻿using System;
using System.Reflection;
using Autofac;
using Xamarin.Forms;

namespace CognizantChat
{
    public class ServiceLocator
    {
        private static ServiceLocator _nstance = new ServiceLocator();
        public static ServiceLocator Instance { get { return _nstance; } }

        private IContainer container;
        private ContainerBuilder containerBuilder;

        private ServiceLocator()
        {
            containerBuilder = new ContainerBuilder();

            var dataAccess = Assembly.GetExecutingAssembly();

            if (AppConfigs.Instance.EnableFake)
            {
                containerBuilder.RegisterAssemblyTypes(dataAccess)
                       .Where(t => t.Name.EndsWith("Service", StringComparison.InvariantCultureIgnoreCase) && t.Name.StartsWith("Fake", StringComparison.InvariantCultureIgnoreCase))
                       .AsImplementedInterfaces();
            }
            else
            {
                containerBuilder.RegisterAssemblyTypes(dataAccess)
                       .Where(t => t.Name.EndsWith("Service", StringComparison.InvariantCultureIgnoreCase) && !t.Name.StartsWith("Fake", StringComparison.InvariantCultureIgnoreCase))
                       .AsImplementedInterfaces();
            }

        }

        public void Build()
        {
            if(DesignMode.IsDesignModeEnabled)
            {
                if (container != null)
                    return;
            }

            container = containerBuilder.Build();
        }

        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}
