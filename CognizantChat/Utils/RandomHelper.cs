﻿using System;
namespace CognizantChat.Utils
{
    public class RandomHelper
    {
        public static string GetRandomName()
        {
            string[] names = { "Anu", "Nirmal", "Kiran", "Humlal", "Raheeb", "Sonya", "Maya", "Gita", "Sita", "Rita", "Sima", "Bina", "Janu", "Manu" };

            int random = new Random().Next(0, names.Length);
            return names[random];
        }
    }
}