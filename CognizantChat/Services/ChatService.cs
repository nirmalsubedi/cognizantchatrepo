﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CognizantChat.Models;
using CognizantChat.Utils;
using SQLite;

namespace CognizantChat.Services
{
    public class ChatService : IChatService
    {
        private readonly SQLiteAsyncConnection sqliteDb;

        public ChatService()
        {
            string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), AppConfigs.OfflineDbName);
            sqliteDb = new SQLiteAsyncConnection(dbPath);
        }

        public async Task<bool> Init()
        {
            Console.WriteLine("Real Chat Service Init()");

            var response = await sqliteDb.DropTableAsync<ChatUser>();
            var result = await sqliteDb.CreateTableAsync<ChatUser>();

            //Load the data into the SQLite Table:
            List<ChatUser> chatUsersDataFromJson = new List<ChatUser>()
            {
                new ChatUser(){ UserId= Guid.NewGuid(), UserName="Anu Rahim", IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName="Test User", IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName="Nirmal Subedi", IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
            };

            var totalCount = await sqliteDb.InsertAllAsync(chatUsersDataFromJson);

            return totalCount > 0;
        }

        public async Task<List<ChatUser>> GetChatUsers(string query=null)
        {
            Console.WriteLine("Real Chat Service GetChatUsers with query : " + query);

            if (!string.IsNullOrEmpty(query))
            {
                var whereQuery = sqliteDb.Table<ChatUser>().Where(f => f.UserName.Contains(query));
                return await whereQuery.ToListAsync();
            }

            return await sqliteDb.Table<ChatUser>().ToListAsync();
        }
    }
}
