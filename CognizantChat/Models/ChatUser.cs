﻿using System;
using SQLite;

namespace CognizantChat.Models
{
    public class ChatUser
    {
        [PrimaryKey]
        public Guid UserId { get; set; }

        public string UserName { get; set; }
        public bool IsSharingVideo { get; set; }

        [Ignore]
        public string LastActive
        {
            get
            {
                return string.Format("{0} hour {1} minutes ago", new Random().Next(1,10), new Random().Next(20,59));
            }
        }
    }
}
