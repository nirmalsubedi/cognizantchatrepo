﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CognizantChat.Models;
using CognizantChat.Utils;

namespace CognizantChat.Services
{
    public class FakeChatService : IChatService
    {
        public FakeChatService()
        {
        }

        public Task<bool> Init()
        {
            Console.WriteLine("Fake Chat Service Init");
            //Just return true nothing to initialize;
            return Task.FromResult(true);
        }

        public Task<List<ChatUser>> GetChatUsers(string query=null)
        {
            //Fake Get Data:
            List<ChatUser> chatUsers = new List<ChatUser>()
            {
                new ChatUser(){ UserId= Guid.NewGuid(), UserName="Anu Rahim", IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName="Test User", IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName="Nirmal Subedi", IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=true},
                new ChatUser(){ UserId= Guid.NewGuid(), UserName=RandomHelper.GetRandomName(), IsSharingVideo=false},
            };

            Console.WriteLine("Fake Chat Service GetChatUsers");

            return Task.FromResult(chatUsers);
        }
    }
}
